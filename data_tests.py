from Src.Data.data_preprocessor import *
from Src.Data.data_generator import AnnotatedDataGenerator

n = 10

settings = ModelSettings()
preprocessData(settings)

generator = AnnotatedDataGenerator(settings, is_validation_generator=True)
x,y = generator.getData(0, n)

printImagesWithBoxes(n, x, y, settings)