from pandas import array
from tqdm import tqdm
from Src.Data.data_utils import *
from Src.const import *
from Src.settings import ModelSettings
import pickle




def verifyPreprocessingComplete(settings:ModelSettings):
    folder,file = getProcessFolderAndFile(settings)
    if not os.path.exists(folder):
        return False
    elif len(os.listdir(folder))==0:
        return False
    elif not os.path.exists(file):
        return False
    else:
        with open(file) as f:
            f_stuff=f.readlines()
            if len(f_stuff)==0 or len(f_stuff)!=len(os.listdir(folder))//2:
                return False

    return True


def preprocessData(settings:ModelSettings):
    errs = 0
    corrs = 0

    DICOM_FOLDER          = f"{DATA_PATH}/Source/Lung-PET-CT-Dx"
    ANNOTATION_FOLDER     = f"{DATA_PATH}/Source/Annotation"

    folder, file = getProcessFolderAndFile(settings)

    if not os.path.exists(folder):
        os.mkdir(folder)

    dirs = os.listdir(ANNOTATION_FOLDER)

    with open(file, "w") as output_file:

        for annotation_id in tqdm(
                dirs,
                desc="[INFO] Processing data",
                total=len(dirs)
            ):

            uid_path = f"{DICOM_FOLDER}/Lung_Dx-{annotation_id}"
            bbox_path = f"{ANNOTATION_FOLDER}/{annotation_id}"

            if not os.path.isdir(uid_path) or not os.path.isdir(bbox_path): continue
            

            try:
                path_dict  = getUID_path(uid_path)           # uid(name) -> (path, name)
                bbox_dict  = processXMLDirectory(bbox_path)  # name -> ndimarray bbox
            except:
                continue

            items = bbox_dict.items()
            for name, bboxes in tqdm(items,desc=f"[INFO] Data folder: {annotation_id}",total=len(items), leave=False):

                try:
                    dcm_path = path_dict[name]
                except:
                    errs += 1
                    continue

                try:
                    img = loadFile(dcm_path)
                    img_resized, bboxes_map = roi2rect(img, bboxes, settings)
                    
                    data_name  = f"{folder}/{corrs}_data.npy"
                    label_name = f"{folder}/{corrs}_label.npy"

                    np.save(data_name, img_resized)
                    np.save(label_name,bboxes_map)

                    output_file.write(f"{data_name} {label_name}\n")

                    corrs += 1
                except Exception as e:
                    print(e)
                    errs += 1
                    continue

        print(f"Corrs: {corrs}, Errs: {errs}, Sum: {corrs+errs}")


