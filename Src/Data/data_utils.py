from cv2 import resize
import numpy as np 
import matplotlib.pyplot as plt
import pydicom as dicomio
import cv2
import numpy as np
import os

from xml.etree import ElementTree
from typing import Dict, Tuple, final
from Src.const import *
from Src.settings import ModelSettings

def roi2rect(img_array:np.ndarray, img_boxes:np.ndarray, settings:ModelSettings) -> Tuple[np.ndarray, np.ndarray]:

    scale_horizontal = img_array.shape[0]/settings.input_shape[0]
    scale_vertical = img_array.shape[1]/settings.input_shape[1]

    resized_img = cv2.resize(src=img_array, dsize=[settings.input_shape[0], settings.input_shape[1]])
    resized_img = np.array(resized_img)

    mn = np.amin(resized_img)
    if(mn<0): resized_img=resized_img-mn
    mx = np.amax(resized_img)
    resized_img = resized_img/mx

    bbox_map = np.zeros(settings.output_shape)


    for i in range(img_boxes.shape[0]):

        bounding_box = img_boxes[i,:]

        xmin = bounding_box[0]/scale_horizontal
        ymin = bounding_box[1]/scale_vertical
        xmax = bounding_box[2]/scale_horizontal
        ymax = bounding_box[3]/scale_vertical
        width = bounding_box[4]/scale_horizontal
        height = bounding_box[5]/scale_vertical

        iterX = settings.input_shape[0]/settings.output_shape[0]
        iterY = settings.input_shape[1]/settings.output_shape[1]
        
        for i in range(settings.output_shape[0]):
            for j in range(settings.output_shape[1]):
                if iterX*i>=xmin and iterX*i<=xmax and iterY*j<=ymax and iterY*j>=ymin:
                    bbox_map[j][i]=1


    resized_img = np.tile(resized_img, (3,1,1))
    resized_img = np.transpose(resized_img, (1,2,0))

    return resized_img, bbox_map


def getUID_path(path:str) -> Dict[str, Tuple[str,str]]:

    def loadFileInformation(filename:str) -> str:
        ds = dicomio.read_file(filename, force=False)
        return ds.SOPInstanceUID #this is name of the file without extension, I think

    dict = {}
    list = os.listdir(path)

    for date in list:
        date_path = os.path.join(path, date)
        series_list = os.listdir(date_path)

        for series in series_list:
            series_path = os.path.join(date_path, series)
            dicom_list = os.listdir(series_path)

            for dicom in dicom_list:
                dicom_path = os.path.join(series_path, dicom)
                dicom_num_info = loadFileInformation(dicom_path)
                dict[dicom_num_info] = dicom_path

    return dict




def matrixToImage(data: np.ndarray) -> np.ndarray:
    data = (data + 1024) * 0.125
    img_rgb = data.astype(np.uint8)
    return img_rgb


def loadFile(filename:str) -> Tuple[np.ndarray, int, int]:
    ds = dicomio.dcmread(filename).pixel_array
    return ds


def processXMLDirectory(data_path:str) -> Dict[str,np.ndarray]:

    data = dict()
    filenames = os.listdir(data_path)

    for filename in filenames:

        tree = ElementTree.parse(os.path.join(data_path, filename))
        root = tree.getroot()

        size_tree = root.find('size')
        objects = root.findall('object')

        width = int(size_tree.find('width').text)
        height = int(size_tree.find('height').text)

        bounding_boxes = np.empty((len(objects),6))
        for idx,object_tree in enumerate(objects):
            for bounding_box in object_tree.iter('bndbox'):

                    xmin = int(bounding_box.find('xmin').text)
                    ymin = int(bounding_box.find('ymin').text)
                    xmax = int(bounding_box.find('xmax').text)
                    ymax = int(bounding_box.find('ymax').text)

            bounding_boxes[idx,:] = np.array([xmin, ymin, xmax, ymax, width, height]) 

        data[filename[:-4]] = bounding_boxes

    return data

def printImagesWithBoxes(count, input, output, settings:ModelSettings):
    
    x_scale = settings.input_shape[0]//settings.output_shape[0]
    y_scale = settings.input_shape[1]//settings.output_shape[1]

    for i in range(count):
        
        img = input[i,:,:,:]
        res = output[i,:,:]

        print(img)

        f, axarr = plt.subplots(1,2)
        axarr[0].imshow(img)

        for x in range(settings.output_shape[0]):
            for y in range(settings.output_shape[1]):
                    if res[x,y] > 0.5:
                        img[x*x_scale:(x+1)*x_scale,y*y_scale:(y+1)*y_scale,:] = np.array([1,0,0])

        axarr[1].imshow(img)
        plt.show(block=True)

def printImagesWithBoxesModel(count, input, output, model_output, settings:ModelSettings):
    
    x_scale = settings.input_shape[0]//settings.output_shape[0]
    y_scale = settings.input_shape[1]//settings.output_shape[1]

    for i in range(count):
        
        img = input[i,:,:,:]
        real_boxes = output[i,:,:]
        model_boxes = model_output[i,:,:]

        f, axarr = plt.subplots(1,3)
        axarr[0].imshow(img)
        
        real_img = img.copy() 
        model_img = img.copy() 

        for x in range(settings.output_shape[0]):
            for y in range(settings.output_shape[1]):
                    if real_boxes[x,y] > 0.5:
                        real_img[x*x_scale:(x+1)*x_scale,y*y_scale:(y+1)*y_scale,:] = np.array([1,0,0])
                    if model_boxes[x,y] > 0.5:
                        model_img[x*x_scale:(x+1)*x_scale,y*y_scale:(y+1)*y_scale,:] = np.array([1,0,0])

        axarr[1].imshow(real_img)
        axarr[2].imshow(model_img)

        plt.show(block=True)

def getProcessFolderAndFile(settings:ModelSettings):
    process_folder = f"{DATA_PATH}/Process_{settings.input_shape[0]}"
    process_file = f"{DATA_PATH}/process_{settings.input_shape[0]}.txt"
    return process_folder, process_file