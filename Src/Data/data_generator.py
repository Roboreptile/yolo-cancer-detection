import random
from typing import List, Tuple
import PIL
from tensorflow.python.keras.utils.all_utils import Sequence

import numpy as np
import pandas as pd

from enum import Enum
from Src.Data.data_utils import getProcessFolderAndFile
from Src.settings import ModelSettings

class GeneratorType(Enum):
    RANDOM = 1
    CSV = 2

class RandomDataGen(Sequence):
    
    def __init__(self,
                n: int,
                batch_size:int,
                input_size:Tuple[int,int,int]=(224, 224, 3),
                output_size:Tuple[int,int] = (10,10)):
        
        self.batch_size = batch_size
        self.input_size = input_size
        self.output_size = output_size

        self.n =int ((n//batch_size)*batch_size)
        if self.n!=n:
            print(f"[WARN] RandomDataGen's sample size adjusted from {n} to {self.n}")
    
    def on_epoch_end(self):
        pass

    def __len__(self):
        return self.n // self.batch_size
    
    def __getitem__(self, idx):
        X = np.random.randint(65_536+1, size=(self.batch_size, *self.input_size))/65_536
        y = np.random.randint(1+1, size=(self.batch_size, *self.output_size))/2

        return X, y


class AnnotatedDataGenerator(Sequence):
    
    def __init__(self, 
                 settings:ModelSettings,
                 is_validation_generator: bool = False,
                 shuffle=True,
                 ):
        
        _,file_name = getProcessFolderAndFile(settings)
        with open(file_name, "r") as paths_file:
            lines = paths_file.readlines()

            if not is_validation_generator:
                n = int(len(lines)*settings.validation_split)
                self.indices = list(range(0, n))
            else:
                n = int(len(lines)*(1-settings.validation_split))
                self.indices = list(range(n,len(lines)-1))

            self.n = n            
            self.data_path:List[str] =  [0]*len(lines)
            self.bbox_path:List[str] =  [0]*len(lines)

            for i, line in enumerate(lines):
                line = line.split(" ")
                self.data_path[i] = line[0]
                self.bbox_path[i] = line[1].replace("\n","")

        self.batch_size = settings.batch_size
        self.input_size = [settings.input_shape[0], settings.input_shape[1], 3]
        self.shuffle = shuffle
        self.output_size = settings.output_shape        
        self.settings = settings

        if self.shuffle:
            self.shuffleIndices()

    def shuffleIndices(self):
        random.shuffle(self.indices)

    
    def getData(self, start_idx, end_idx):

        indices     =  self.indices[start_idx:end_idx]

        X_batch = np.zeros([len(indices), self.input_size[0], self.input_size[1], self.input_size[2]])
        Y_batch = np.zeros([len(indices), self.output_size[0], self.output_size[0]])

        for i,idx in enumerate(indices):
            image = np.load(f"{self.data_path[idx]}")
            bbox = np.load(f"{self.bbox_path[idx]}")

            X_batch[i, :, :, :] = image
            Y_batch[i, :, :] = bbox

        return X_batch, Y_batch
    
    # Overload
    def __getitem__(self, index):

        if (index+1)*self.batch_size>=self.n:
            X,y = self.getData(index * self.batch_size, -1)
        else:
            X,y = self.getData(index * self.batch_size, (index + 1) * self.batch_size)

        return X, y
    
    # Overload
    def __len__(self):
        return self.n // self.batch_size

    # Overload
    def on_epoch_end(self):
        if self.shuffle:
            self.shuffleIndices()
