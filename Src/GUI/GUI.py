from turtle import settiltangle
from typing import List

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QGridLayout, QVBoxLayout, QFrame, QPushButton,
                             QVBoxLayout, QHBoxLayout, QFileDialog, QLabel,
                             QPushButton,QSpinBox, QDialog, QLineEdit,
                             QCheckBox, QDialogButtonBox, QWidget, QProgressBar,
                             QListView, QTextBrowser, QApplication, QSpacerItem,
                             QSizePolicy, QListWidget, QListWidgetItem, QComboBox,
                             QGroupBox, QLayout)

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

import matplotlib.pyplot as plt
from matplotlib.figure import Figure
import random
import sys

from Src.GUI.stylesheets import *
from Src.GUI.utils import showMessageBox
from Src.GUI.validators import *
from Src.AI.model_logic import *
from Src.settings import *

class describedWidget:
    def __init__(self, widget:QWidget, description="", tooltip=""):
        self.widget = widget
        self.description = description
        self.tooltip = tooltip

class ClassifierGUI(object):

    # ==== VISUALIZATION ======================
    visualizationAndProgressLayout:QVBoxLayout
    # Plot figure
    figure:Figure
    canvas:FigureCanvas
    # Progress bar
    progressBar:QProgressBar
    progressBarLabel:QLabel
    # Left buttons
    startTrainingButton:QPushButton
    loadModelButton:QPushButton
    testModelButton:QPushButton

    # ==== SETTINGS ======================
    configurationAndConsoleLayout:QVBoxLayout
    # Config
    modelNameTextBox:QLineEdit
    epochsTextBox:QLineEdit
    batchSizeTextBox:QLineEdit
    learningRateTextBox:QLineEdit
    validationSplitTextBox:QLineEdit
    optimizerTextBox:QComboBox
    # Console
    verboseCheckBox:QCheckBox
    printSummaryCheckBox:QCheckBox
    consoleText:QTextBrowser

    # ==== LAYERS ======================
    layersConfigurationLayout:QVBoxLayout
    # In
    inputShapeTextBox:QLineEdit
    # Conv layers
    convLayersList:QVBoxLayout
    convLayersButton:QPushButton
    convLayersMxButton:QPushButton
    # Middle layers
    middleLayerTextBox:QComboBox
    # Dense layers
    denseLayersList:QVBoxLayout
    denseLayersButton:QPushButton
    # Out
    outputShapeTextBox:QLineEdit
    reconstructModelButton:QPushButton
    launchPreprocessingButton:QPushButton

    # ==== OTHER =============
    app:QApplication
    settings:ModelSettings
    convCache:List[ConvLayer]
    denseCache:List[DenseLayer]

    # Stats
    loss:List[float]
    val_loss:List[float]
    acc:List[float]
    val_acc:List[float]
    
    def __init__(self):

        self.app = QApplication(sys.argv)
        self.settings = ModelSettings()
        self.convCache = []
        self.denseCache = []
        self.loss = []
        self.val_loss = []
        self.acc = []
        self.val_acc = []


        self.buildMainDialog()
        self.buildVisualizationLayout()
        self.buildConfigurationLayout()
        self.buildLayersLayout()

        self.mainDialog.show()

        sys.exit(self.app.exec_())

    def buildMainDialog(self):
        self.mainDialog = QDialog()
        self.mainDialog.setWindowTitle("Classifier GUI")
        self.mainDialog.setWindowModality(Qt.NonModal)
        self.mainDialog.setMinimumSize(1500, 500)
        self.mainDialog.setStyleSheet(APPLICATION_STYLESHEET)
        self.mainLayout = QHBoxLayout(self.mainDialog)

    def buildVisualizationLayout(self):
        self.visualizationAndProgressLayout = QVBoxLayout()

        self.buildPlotWidget()
        self.buildProgressBar()
        self.buildTrainingButtons()

        self.mainLayout.addLayout(self.visualizationAndProgressLayout, 2)

    def buildConfigurationLayout(self):
        self.configurationAndConsoleLayout = QVBoxLayout()

        self.buildSettingsLayout()
        self.buildConsole()

        self.mainLayout.addLayout(self.configurationAndConsoleLayout, 1)


    def buildPlotWidget(self):
        visualizationWidget = QGroupBox()
        visualizationWidget.setStyleSheet(VISUALIZATION_PANEL_STYLESHEET)

        plotLayout = QVBoxLayout()
        plotLayout.setAlignment(Qt.AlignmentFlag.AlignCenter)
        visualizationWidget.setLayout(plotLayout)   

        self.figure = plt.figure(figsize=(8,8))
        self.canvas = FigureCanvas(self.figure)
        toolbar = NavigationToolbar(self.canvas, None)
        toolbar.setStyleSheet(TOOLBAR_STYLESHEET)

        plotLayout.addWidget(toolbar)
        plotLayout.addWidget(self.canvas)

        self.visualizationAndProgressLayout.addWidget(visualizationWidget, 8)

    def buildProgressBar(self):
        self.progressBar = QProgressBar(minimum = 0, maximum = 100, value = 10)
        self.progressBarLabel = QLabel("This will describe what is happening right now\n(What process is running)")

        self.visualizationAndProgressLayout.addWidget(self.progressBar, 1)
        self.visualizationAndProgressLayout.addWidget(self.progressBarLabel, 1)


    def buildTrainingButtons(self):
        buttonsLayout = QHBoxLayout()
        self.startTrainingButton = QPushButton("Start training")
        self.startTrainingButton.clicked.connect(self.startTraining)
        self.startTrainingButton.setStyleSheet(BLUE_BUTTON_STYLESHEET)
        self.startTrainingButton.setEnabled(False)

        self.loadModelButton = QPushButton("Load model")
        self.loadModelButton.clicked.connect(self.loadModel)
        self.loadModelButton.setStyleSheet(BLUE_BUTTON_STYLESHEET)

        self.testModelButton = QPushButton("Test on an image...")
        self.testModelButton.clicked.connect(self.testModel)
        self.testModelButton.setStyleSheet(BLUE_BUTTON_STYLESHEET)

        buttonsLayout.addWidget(self.startTrainingButton)
        buttonsLayout.addWidget(self.loadModelButton)
        buttonsLayout.addWidget(self.testModelButton)

        self.visualizationAndProgressLayout.addLayout(buttonsLayout, 1)


    def buildSettingsLayout(self):
        self.modelNameTextBox = QLineEdit()
        self.modelNameTextBox.setText(str(self.settings.model_name))
        self.modelNameTextBox.setStyleSheet(TEXTBOX_STYLESHEET)

        self.epochsTextBox = QLineEdit()
        self.epochsTextBox.setText(str(self.settings.epochs))
        self.epochsTextBox.setValidator(NUMBER_VALIDATOR)
        self.epochsTextBox.setStyleSheet(TEXTBOX_STYLESHEET)

        self.batchSizeTextBox = QLineEdit()
        self.batchSizeTextBox.setText(str(self.settings.batch_size))
        self.batchSizeTextBox.setValidator(NUMBER_VALIDATOR)
        self.batchSizeTextBox.setStyleSheet(TEXTBOX_STYLESHEET)

        self.learningRateTextBox = QLineEdit()
        self.learningRateTextBox.setText(str(self.settings.learning_rate))
        self.learningRateTextBox.setValidator(FRACTION_VALIDATOR)
        self.learningRateTextBox.setStyleSheet(TEXTBOX_STYLESHEET)

        self.validationSplitTextBox = QLineEdit()
        self.validationSplitTextBox.setText(str(self.settings.validation_split))
        self.validationSplitTextBox.setValidator(FRACTION_VALIDATOR)
        self.validationSplitTextBox.setStyleSheet(TEXTBOX_STYLESHEET)

        self.optimizerTextBox = QComboBox()
        self.optimizerTextBox.addItems([str(item) for item in OptimizerType])
        self.optimizerTextBox.setStyleSheet(TEXTBOX_STYLESHEET)

        self.addDescribedWidget(parent=self.configurationAndConsoleLayout, listWidgets=[
            describedWidget(
                widget=self.modelNameTextBox,
                description="Name:",
                tooltip="Name of the model"
            ),
            describedWidget(
                widget=self.epochsTextBox,
                description="Epochs:",
                tooltip="Number of epochs, must be an int"
            ),
            describedWidget(
                widget=self.batchSizeTextBox,
                description="Batch size:",
                tooltip="Number of images in a single batch, must be an int"
            ),
            describedWidget(
                widget=self.learningRateTextBox,
                description="Learning rate:",
                tooltip="Speed of gradient descent, must be 0.001 - 0.1"
            ),
            describedWidget(
                widget=self.validationSplitTextBox,
                description="Validation split:",
                tooltip="Percentage of images used for validation, 0.05-0.95"
            ),
            describedWidget(
                widget=self.optimizerTextBox,
                description="Optimizer:",
                tooltip="TODO enforce specific values"
            )
        ])

    def buildConsole(self):
        consoleSettingsLayout = QHBoxLayout()
        self.verboseCheckBox = QCheckBox()
        self.verboseCheckBox.setChecked(False)

        self.printSummaryCheckBox = QCheckBox()
        self.printSummaryCheckBox.setChecked(False)
        
        consoleSpacer = QSpacerItem(10, 50, QSizePolicy.Minimum, QSizePolicy.Expanding)

        consoleLabel = QLabel("Console output")
        consoleLabel.setStyleSheet("font-size: 12px; font-weight: bold;")
        self.consoleText = QTextBrowser()
        self.consoleText.setStyleSheet("background-color: black; color: white;")
        self.consoleText.setLineWidth(CONSOLE_LN_WDTH)
        self.consoleText.setText("This will show console output")

        self.configurationAndConsoleLayout.addItem(consoleSpacer)
        self.configurationAndConsoleLayout.addWidget(consoleLabel, 1)
        self.configurationAndConsoleLayout.addWidget(self.consoleText, 3)

        self.addDescribedWidget(parent=consoleSettingsLayout, listWidgets=[
            describedWidget(
                widget=self.verboseCheckBox,
                description="TF Verbose:",
                tooltip="TODO"
            ),
            describedWidget(
                widget=self.printSummaryCheckBox,
                description="Print summary:",
                tooltip="Print model summary in console"
            )
        ])

        self.configurationAndConsoleLayout.addLayout(consoleSettingsLayout,2)

    def buildLayersLayout(self):
        self.layersConfigurationLayout = QVBoxLayout()
       
        self.buildInputShapeBox()
        self.buildConvLayersList()
        self.buildMiddleLayerSelector()
        self.buildDenseLayersList()
        self.buildOutputShapeBoxAndReconstructButton()

        self.mainLayout.addLayout(self.layersConfigurationLayout, 1)

    def buildInputShapeBox(self):
        self.inputShapeTextBox = QLineEdit()
        self.inputShapeTextBox.setText(f"{self.settings.input_shape[0]}")
        self.inputShapeTextBox.setMaxLength(3)
        self.inputShapeTextBox.setMaximumWidth(45)
        self.inputShapeTextBox.setValidator(NUMBER_VALIDATOR)
        self.inputShapeTextBox.textChanged.connect(lambda s:self.computeConvLabels(s))
        self.inputShapeTextBox.setStyleSheet(TEXTBOX_STYLESHEET)

        self.addDescribedWidget(parent=self.layersConfigurationLayout, listWidgets=[
            describedWidget(
                widget=self.inputShapeTextBox,
                description="Input shape:",
                tooltip="TODO"
            )]
        )

    def buildOutputShapeBoxAndReconstructButton(self):
        self.outputShapeTextBox = QLineEdit()
        self.outputShapeTextBox.setText(f"{self.settings.output_shape[0]}")
        self.outputShapeTextBox.setMaxLength(3)
        self.outputShapeTextBox.setMaximumWidth(45)
        self.outputShapeTextBox.setValidator(NUMBER_VALIDATOR)
        self.outputShapeTextBox.setStyleSheet(TEXTBOX_STYLESHEET)

        self.addDescribedWidget(parent=self.layersConfigurationLayout, listWidgets=[
            describedWidget(
                widget=self.outputShapeTextBox,
                description="Output shape:",
                tooltip="TODO"
            )]
        )

        self.reconstructModelButton = QPushButton("Reconstruct model")
        self.reconstructModelButton.clicked.connect(self.reconstructModel)
        self.reconstructModelButton.setStyleSheet(BLUE_BUTTON_STYLESHEET)
        self.layersConfigurationLayout.addWidget(self.reconstructModelButton)

        self.launchPreprocessingButton = QPushButton("Preprocess data for set input size")
        self.launchPreprocessingButton.clicked.connect(self.preprocessData)
        self.launchPreprocessingButton.setStyleSheet(BLUE_BUTTON_STYLESHEET)
        self.layersConfigurationLayout.addWidget(self.launchPreprocessingButton)

    def buildConvLayersList(self):
        convLayersLabel = QLabel("Convolutional Layers")
        convLayersListWidget = QWidget()
        convLayersListWidget.setStyleSheet(LAYERS_LIST_WIDGET_STYLESHEET)
        self.convLayersList = QVBoxLayout(convLayersListWidget)

        self.convLayersButton = QPushButton("+ Convolutional")
        self.convLayersButton.clicked.connect(self.addConvLayer)
        self.convLayersButton.setStyleSheet(BLUE_BUTTON_STYLESHEET)

        self.convLayersMxButton = QPushButton("+ MaxPool")
        self.convLayersMxButton.clicked.connect(self.addMaxPoolLayer)
        self.convLayersMxButton.setStyleSheet(GREEN_BUTTON_STYLESHEET)

        self.convLayersList.addWidget(self.convLayersButton)
        self.convLayersList.addWidget(self.convLayersMxButton)
        self.convLayersList.addStretch()

        self.layersConfigurationLayout.addWidget(convLayersLabel)
        self.layersConfigurationLayout.addWidget(convLayersListWidget,5)

    def buildMiddleLayerSelector(self):
        middleLayerLabel = QLabel("Middle layer")
        self.middleLayerTextBox = QComboBox()
        self.middleLayerTextBox.addItems([str(item) for item in MiddleLayerType])
        self.middleLayerTextBox.setCurrentText(str(self.settings.middle_layer))
        self.middleLayerTextBox.setStyleSheet(TEXTBOX_STYLESHEET)

        self.layersConfigurationLayout.addWidget(middleLayerLabel)
        self.layersConfigurationLayout.addWidget(self.middleLayerTextBox)

    def buildDenseLayersList(self):
        denseLayersLabel = QLabel("Dense Layers")
        denseLayersListWidget = QWidget()
        denseLayersListWidget.setStyleSheet(LAYERS_LIST_WIDGET_STYLESHEET)
        self.denseLayersList = QVBoxLayout(denseLayersListWidget)

        self.denseLayersButton = QPushButton("+ Dense")
        self.denseLayersButton.clicked.connect(self.addDenseLayer)
        self.denseLayersButton.setStyleSheet(BLUE_BUTTON_STYLESHEET)

        self.denseLayersList.addWidget(self.denseLayersButton)
        self.denseLayersList.addStretch()

        self.layersConfigurationLayout.addWidget(denseLayersLabel)
        self.layersConfigurationLayout.addWidget(denseLayersListWidget,5)

    def reconstructModel(self):
        self.createModelSettings()
        self.clearConsole()
        
        reconstructModel(self.settings, self.updateConsole)

        self.startTrainingButton.setEnabled(True)

    def startTraining(self):
        if not verifyData(self.settings):
            showMessageBox("Preprocessing error", f"Run preprocessing for {self.settings.input_shape[0]} image size", "Critical")
            return
        self.loss = []
        self.acc = []
        self.val_loss = []
        self.val_acc = []

        startModelTraining(
            self.settings, 
            self.updateConsole, 
            self.plot,
            self.figure
        )

    def loadModel(self):
        dir = QFileDialog.getOpenFileName(
            self.mainDialog,
            "Choose Saved Model", 
            "./", "Model files (*.h5)"
        )[0]

        print(dir)

    def testModel(self):
        dir = QFileDialog.getOpenFileName(
            self.mainDialog,
            "Choose Saved Model", 
            "./", "Model files (*.h5)"
        )[0]

        print(dir)

    def preprocessData(self) -> None:
        if verifyData(self.settings):
            showMessageBox(
                "Preprocessing already completed", 
                f"Preprocessing for {self.settings.input_shape[0]} was already completed. Rerun?", 
                "Question",
                True, 
                ["Yes", "No"], 
                preprocessModelData,
                self.settings
            )
        else:
            preprocessModelData(self.settings)
        
    def clearConsole(self) -> None:
        self.consoleText.setText("")

    def updateConsole(self, data:str) -> None:
        self.consoleText.setText(f"{self.consoleText.toPlainText()}\n{data}")
        self.consoleText.repaint()

    def plot(self, logs = None, epochs = 0):
          
        if logs==None: 
            return

        epochs_range = range(len(self.loss)+1)
        self.figure.clear()

        print(logs)
        self.loss.append(logs['loss'])
        self.acc.append(logs['accuracy'])
        self.val_loss.append(logs['val_loss'])
        self.val_acc.append(logs['val_accuracy'])

        # Creates 2 plots on one graph
        # First one represents the changing accuracy over epochs
        ax1 = self.figure.add_subplot(1, 2, 1)
        ax1.plot(epochs_range, self.acc, label='Training Accuracy')
        ax1.plot(epochs_range, self.val_acc, label='Validation Accuracy')
        #ax1.xlabel('Epochs')
        #ax1.ylabel('Accuracy')
        ax1.legend(loc='upper left')
        #ax1.title('Accuracy')

        # Second one represents the changing loss over epochs
        ax2 = self.figure.add_subplot(1, 2, 2)
        ax2.plot(epochs_range, self.loss, label='Training Loss')
        ax2.plot(epochs_range, self.val_loss, label='Validation Loss')
        #ax2.xlabel('Epochs')
        #ax2.ylabel('Loss')
        ax2.legend(loc='upper left')
        #ax2.title('Loss')
  
        # refresh canvas
        self.canvas.draw()

        self.canvas.repaint()

    def createModelSettings(self):
        x = int(self.inputShapeTextBox.text())
        y = int(self.outputShapeTextBox.text())

        self.settings = ModelSettings(
            convolution_layers=self.convCache,
            middle_layer=MiddleLayerType[self.middleLayerTextBox.currentText().split(".")[1]],
            dense_layers=self.denseCache,
            input_shape=[x,x],
            output_shape=[y,y],
            optimizer=OptimizerType[self.optimizerTextBox.currentText().split(".")[1]],
            learning_rate=float(self.learningRateTextBox.text()),
            epochs = int(self.epochsTextBox.text()),
            batch_size= int(self.batchSizeTextBox.text()),
            validation_split = float(self.validationSplitTextBox.text()),
            model_name = self.modelNameTextBox.text(),
            print_summary = self.printSummaryCheckBox.isChecked(),
            verbose = self.verboseCheckBox.isChecked()
        )

        print(self.settings)

    def addConvLayer(self):
        w = QWidget()
        w.setFixedHeight(50)
        w.setStyleSheet(LAYERS_LIST_ELEMENT_STYLESHEET)

        l = QHBoxLayout(w)

        auto_l = QLabel()
        obj = ConvLayer()
        obj.label = auto_l

        ctextboxl = QLabel("Filters:")
        ctextbox = QLineEdit()
        ctextbox.setText("32")
        ctextbox.setMaxLength(3)
        ctextbox.setMaximumWidth(45)
        ctextbox.setValidator(NUMBER_VALIDATOR)
        ctextbox.setStyleSheet(TEXTBOX_STYLESHEET)
        ctextbox.textChanged.connect(lambda s:self.computeConvLabels(s))
        obj._tf = ctextbox
        ctextboxl.setBuddy(ctextbox)

        stextboxl = QLabel("Shape:")
        stextbox = QLineEdit()
        stextbox.setText("3")
        stextbox.setMaxLength(2)
        stextbox.setMaximumWidth(30)
        stextbox.setValidator(NUMBER_VALIDATOR)
        stextbox.setStyleSheet(TEXTBOX_STYLESHEET)
        stextbox.textChanged.connect(lambda s:self.computeConvLabels(s))
        obj._ts = stextbox
        stextboxl.setBuddy(stextbox)

        ttextboxl = QLabel("Stride:")
        ttextbox = QLineEdit()
        ttextbox.setText("1")
        ttextbox.setMaxLength(1)
        ttextbox.setMaximumWidth(30)
        ttextbox.setValidator(NUMBER_VALIDATOR)
        ttextbox.setStyleSheet(TEXTBOX_STYLESHEET)
        ttextbox.textChanged.connect(lambda s:self.computeConvLabels(s))
        obj._tst = ttextbox
        ttextboxl.setBuddy(ttextbox)

        l.setAlignment(Qt.AlignmentFlag.AlignHCenter)
        l.addWidget(ctextboxl)
        l.addWidget(ctextbox)
        l.addWidget(stextboxl)
        l.addWidget(stextbox)
        l.addWidget(ttextboxl)
        l.addWidget(ttextbox)

        l.addWidget(auto_l)

        btn = QPushButton("Remove")
        btn.setStyleSheet(RED_BUTTON_STYLESHEET)
        btn.clicked.connect(lambda:self.deleteConvLayer(w, obj))

        l.addWidget(btn)        

        self.convLayersList.insertWidget(len(self.convCache),w)
        self.convCache.append(obj)
        self.computeConvLabels(0)   


    def deleteConvLayer(self, w:QWidget, obj:Layer):
        w.hide()
        self.convLayersList.removeWidget(w)
        self.convCache.remove(obj)
        self.computeConvLabels(0)   


    def addMaxPoolLayer(self):
        w = QWidget()
        w.setFixedHeight(50)
        w.setStyleSheet(LAYERS_LIST_SPECIAL_STYLESHEET)

        l = QHBoxLayout(w)

        labell = QLabel("Max Pool")
        obj = MaxPool()
        label = QLabel()
        obj.label = label
        labell.setBuddy(label)
        l.addWidget(labell)
        l.addWidget(label)


        btn = QPushButton("Remove")
        btn.setStyleSheet(RED_BUTTON_STYLESHEET)
        btn.clicked.connect(lambda:self.deleteMaxPoolLayer(w, obj))

        l.addWidget(btn)    
        self.convLayersList.insertWidget(len(self.convCache),w)
        self.convCache.append(obj) 

        self.computeConvLabels(0)   

    def deleteMaxPoolLayer(self, w:QWidget, obj:Layer):
        w.hide()
        self.convLayersList.removeWidget(w)
        self.convCache.remove(obj)
        self.computeConvLabels(0)   


    def addDenseLayer(self):
        w = QWidget()
        w.setFixedHeight(50)
        w.setStyleSheet(LAYERS_LIST_ELEMENT_STYLESHEET)

        l = QHBoxLayout(w)
        obj = DenseLayer()


        ctextboxl = QLabel("Neurons:")
        ctextbox = QLineEdit()
        ctextbox.setText("1024")
        ctextbox.setMaxLength(5)
        ctextbox.setMaximumWidth(60)
        ctextbox.setValidator(NUMBER_VALIDATOR)
        ctextbox.setStyleSheet(TEXTBOX_STYLESHEET)
        obj._tn = ctextbox
        ctextboxl.setBuddy(ctextbox)

        stextboxl = QLabel("Dropout:")
        stextbox = QLineEdit()
        stextbox.setText("0.1")
        stextbox.setMaxLength(3)
        stextbox.setMaximumWidth(30)
        stextbox.setValidator(FRACTION_VALIDATOR)
        stextbox.setStyleSheet(TEXTBOX_STYLESHEET)
        obj._td = stextbox
        stextboxl.setBuddy(stextbox)

        l.setAlignment(Qt.AlignmentFlag.AlignHCenter)
        l.addWidget(ctextboxl)
        l.addWidget(ctextbox)
        l.addWidget(stextboxl)
        l.addWidget(stextbox)


        btn = QPushButton("Remove")
        btn.setStyleSheet(RED_BUTTON_STYLESHEET)
        btn.clicked.connect(lambda:self.deleteDenseLayer(w, obj))

        l.addWidget(btn)        

        self.denseLayersList.insertWidget(len(self.denseCache),w)
        self.denseCache.append(obj)

    def deleteDenseLayer(self, w:QWidget, obj:Layer):
        w.hide()
        self.denseLayersList.removeWidget(w)
        self.denseCache.remove(obj)

    def computeConvLabels(self, _):

        for idx,layer in enumerate(self.convCache):
            if idx == 0:
                x = int(self.inputShapeTextBox.text())
                layer.input_shape = [x,x,3]
            else:
                layer.input_shape = self.convCache[idx-1].output_shape
            
            if not layer.is_max:
                t = int(layer._tf.text()) if layer._tf.text() != "" else 0
                third = t
                ts = int(layer._ts.text()) if layer._ts.text() != "" else 1
                top = layer.input_shape[0]-(ts-1) 
                tst =  int(layer._tst.text()) if layer._tst.text() != "" else 1
                stride = tst
                first_and_second = top//stride
                layer.label.setText(f"[{layer.input_shape[0]},{layer.input_shape[1]},{layer.input_shape[2]}] -> [{first_and_second},{first_and_second},{third}]")
                layer.label.setStyleSheet(OK_TEXT_STYLESHEET if top % stride == 0 else WARN_TEXT_STYLESHEET)
                layer.output_shape = [first_and_second, first_and_second, third]
            else:
                third = int(layer.input_shape[2])
                first_and_second = layer.input_shape[0]//2
                layer.label.setText(f"[{layer.input_shape[0]},{layer.input_shape[1]},{layer.input_shape[2]}] -> [{first_and_second},{first_and_second},{third}]")
                layer.label.setStyleSheet(OK_TEXT_STYLESHEET if  layer.input_shape[0] % 2 == 0 else WARN_TEXT_STYLESHEET)
                layer.output_shape = [first_and_second, first_and_second, third]

    def addDescribedWidget(self, parent, listWidgets: List[describedWidget], align=Qt.AlignLeft):
        layout = QGridLayout()
        row = 0
        for widget in listWidgets:
            label = QLabel(widget.description)
            if widget.description != "":
                label.setBuddy(widget.widget)
                layout.addWidget(label, row, 0)
            layout.addWidget(widget.widget, row, 1)
            if widget.tooltip != "":
                widget.widget.setToolTip(widget.tooltip)
                label.setToolTip(widget.tooltip)
            row += 1
        layout.setAlignment(align)
        parent.addLayout(layout)
        return layout