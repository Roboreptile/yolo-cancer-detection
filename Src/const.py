from os import environ

####
#### This file contains constant values for the whole program
####

GRAPHS_PATH     = './Graphs'                   # Folder for where to store graphs
DATA_PATH       = './Data'             # Folder containing multiple folders with dog images
MODELS_PATH     = './Models'                   # Folder for where to store models after training

OUTPUTS_PATH    = './Processed_Images'         # Folder containing the augmented doggos images
PREPRC_PATH     = './Processed_Paths'          # Folder containing files with paths and breeds of the dogs
STATS_FILE      = 'models_statistics.csv'       # File which stores statistics about every session
TEST_FILE       = 'test_set_statistics.csv'     # File which stores statistics of a test session run on a selected model


DEBUG           = False                         # Debug mode - loads/processes only a few images basically instant program execution
CONSOLE_LN_WDTH = 40                            # TF model's summary configuration parameter
DELIMETER       = ";"                           # CSV delimeter

if not DEBUG:
    # This silences obnoxious tensorflow messages
    environ['TF_CPP_MIN_LOG_LEVEL'] = "1"
else:
    environ['TF_CPP_MIN_LOG_LEVEL'] = "0"




