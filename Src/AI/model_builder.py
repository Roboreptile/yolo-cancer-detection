import tensorflow.python as tf
from tensorflow.python.keras import layers
from tensorflow.python.keras import Sequential
from tensorflow.python.keras.callbacks import History
from tensorflow.python.keras.utils.all_utils import Sequence
from tensorflow.python.keras.losses import CategoricalCrossentropy
from tensorflow.python.keras.callbacks import Callback
from tensorflow.python.keras.optimizers import *


import numpy as np
from time import time
from typing import Callable, Tuple, TypedDict
import matplotlib.pyplot as plt

from Src.const import *
from Src.settings import *

def create_model(settings: ModelSettings) -> Sequential :
    '''
    Creates a custom model based n input arguments.\n
    Model's structure: Input layer -> Reuqested Conv2Ds -> Middle layer -> Dense layers -> Output layer.\n
    Output layer is a Dense layer with softmax activation.
    '''
    model = Sequential()

    # Input layer
    model.add(layers.InputLayer(input_shape=[settings.input_shape[0],settings.input_shape[1],3]))

    # if settings.convolution_activation == ActivationType.relu:
    #         convolution_activation = 'relu'
    # elif settings.convolution_activation == ActivationType.tanh:
    #     convolution_activation = 'tanh'
    convolution_activation = 'relu'

    # Convolutional layers builder - Conv2D layer(s) followed by MaxPool2D. 
    for idx,layer in enumerate(settings.convolution_layers):

        if not layer.is_max:
            model.add(layers.Conv2D(
                filters = int(layer._tf.text()),
                kernel_size = (int(layer._ts.text()), int(layer._ts.text())),
                strides = (int(layer._tst.text()), int(layer._tst.text())),  
                padding = 'valid',
                activation = convolution_activation
            ))
        else: 
            # Adds MaxPool2D after requested Conv2D layers
            # Does not add after the last layer, as some networks 
            # use different layer as a connector
            model.add(layers.MaxPool2D(
                pool_size = (2,2), 
                strides = (2,2)
            ))
        
    # Middle layer - connection between Conv2Ds and Dense
    # Flattens the shape from 4D to 2D
    if settings.middle_layer == MiddleLayerType.average_pool:
        model.add(layers.GlobalAveragePooling2D(
            data_format='channels_last'
        ))
    elif settings.middle_layer == MiddleLayerType.max_pool:
        model.add(layers.MaxPool2D(
            pool_size = (2,2), 
            strides = (2,2)
        ))
        model.add(layers.Flatten(
            data_format='channels_last'
        ))
    elif settings.middle_layer == MiddleLayerType.flatten:
        model.add(layers.Flatten(
            data_format='channels_last'
        ))
    else:
        print(f"Unknown middle layer type: {settings.middle_layer}")

    
    # if settings.dense_activation == ActivationType.relu:
    #     dense_activation = 'relu'
    # elif settings.dense_activation == ActivationType.tanh:
    #     dense_activation = 'tanh'
    dense_activation = 'relu'

    # Dense layers builder - Dense layer(s) followed by Dropout.
    for idx,layer in enumerate(settings.dense_layers):
        model.add(layers.Dense(
            units = int(layer._tn.text()),
            activation = dense_activation
        ))
        d = float(layer._td.text())
        # Adds dropout after adding requested dense layers
        if d != 0:
            model.add(layers.Dropout(
                rate = d
            ))
    
        
    # Output layer - dense layer that will output an array 
    # with breed predictions
    model.add(layers.Dense(
        settings.output_shape[0]*settings.output_shape[1],
        activation='softmax'
    ))

    model.add(layers.Reshape(
        settings.output_shape
    ))
    
    return model


def compile_model(model:Sequential, settings: ModelSettings, print_fn:Callable[[str],None]) -> Sequential:
    '''
    Compiles the model with selected optimizer (adam/SGD/RMSProp) and learning rate.\n
    Uses 'accuracy' as a measure statistic, Categorical Cross Entropy loss function. 
    '''

    if(settings.optimizer==OptimizerType.adam):
        opt = adam_v2.Adam(learning_rate=settings.learning_rate)
    elif(settings.optimizer==OptimizerType.SGD):
        opt = gradient_descent_v2.SGD(learning_rate=settings.learning_rate)
    elif(settings.optimizer==OptimizerType.RMSProp):
        opt = rmsprop_v2.RMSprop(learning_rate=settings.learning_rate)
    elif(settings.optimizer==OptimizerType.nadam):
        opt = nadam_v2.Nadam(learning_rate=settings.learning_rate)
    else:
        print(f"Unknown optimier: {settings.optimizer}")

    model.compile(
        optimizer = opt,
        loss = CategoricalCrossentropy(from_logits=True),
        metrics = ['accuracy']
    )

    # Prints summary of the model, mostly for debug
    #if settings.print_summary: 
    model.summary(line_length=CONSOLE_LN_WDTH ,print_fn=print_fn)

    return model

def train_model(model:Sequential, 
                train_generator:Sequence, 
                valid_generator:Sequence, 
                settings: ModelSettings,
                graphing_func: Callable[[TypedDict], None]) -> Tuple[Sequential, History]:
    '''
    Trains the model using provided images and labels.
    '''
    class MyCallback(Callback):
        def on_epoch_end(self, epoch, logs=None):
            graphing_func(logs, epoch)

    if(settings.verbose == 0): 
        print("[INFO] Training in progress...")

    history = model.fit(
        train_generator,
        validation_data=valid_generator,
        epochs=settings.epochs,
        verbose=settings.verbose,
        callbacks=[MyCallback()]
    )

    return (model, history)

def save_statistics(history: History, settings: ModelSettings) -> Tuple[str,str]:
    '''
    Loads training statistics from the model, and saves them to STATS_FILE.
    '''
    # Extract interesting statistics
    rounded_acc = round(history.history['val_accuracy'][-1],3)
    rounded_loss = round(history.history['val_loss'][-1],3)

    id = str(time())
    # Generate names for the model and object
    model_name =  f"{settings.model_name}-{settings.input_shape[0]}x{settings.input_shape[1]}-acc {rounded_acc:.3f}-id {id}.h5"
    graph_name =  f'graph-acc {rounded_acc:.3f}-'+id+'.png'

    # Write statistics to file
    f = open(STATS_FILE, 'a')
    f.write(DELIMETER.join([
        model_name,
        graph_name,
        f"{rounded_acc:.3f}",
        f"{rounded_loss:.3f}",
        str(settings),
        "\n"
    ]))
    f.close()

    # Returns generated names
    return (f"{MODELS_PATH}/{model_name}", f"{GRAPHS_PATH}/{graph_name}")

def save_model(model:Sequential, model_name: str) -> None:
    '''
    Saves the model into the MODELS folder.
    '''

    model.save(model_name)
    print(f"[INFO] Saved model at {model_name}")


def load_model(model_path:str) -> Sequential:
    '''
    Loads the model from the path provided.
    '''
    model = tf.keras.models.load_model(model_path)
    model.summary()
    return model


def make_a_guess(model:Sequential, images:np.array) -> np.array:
    '''
    By using a given trained model, predicts the output(s) for the input image(s).\n
    Note that this has to be a 4D array, so 3D image must be reshaped into [1,image.shape].
    '''
    return model(images, training = False)


