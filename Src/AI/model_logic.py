from Src.AI.model_builder import  *
from Src.Data.data_generator import *
from Src.Data.data_preprocessor import preprocessData, verifyPreprocessingComplete
from Src.Data.data_utils import printImagesWithBoxes, printImagesWithBoxesModel
from Src.settings import *

from tensorflow.python.keras.utils.all_utils import Sequence  
from tensorflow.python.keras.models import Sequential 

model:Sequential

def preprocessModelData(settings: ModelSettings) -> None:
    preprocessData(settings)

def verifyData(settings: ModelSettings) -> bool:
    return verifyPreprocessingComplete(settings)

def makeGenerators(generatorType:GeneratorType, settings:ModelSettings) -> Tuple[Sequence, Sequence]:

    if generatorType == generatorType.CSV:
        return (
            AnnotatedDataGenerator(
                settings
            ), 
            AnnotatedDataGenerator(
                settings,
                is_validation_generator=True
            ),
        )
    elif generatorType == generatorType.RANDOM:
        n = 1000
        return (
            RandomDataGen(
                int(n*(1-settings.validation_split)),
                settings.batch_size,
                [settings.input_shape[0],settings.input_shape[1],3],
                settings.output_shape,
            ),
            RandomDataGen(
                n*settings.validation_split,
                settings.batch_size,
                [settings.input_shape[0],settings.input_shape[1],3],
                settings.output_shape,
            ),
        )
def reconstructModel(settings:ModelSettings,
                printfn:Callable[[str],None] = print):

    global model
    model = create_model(
            settings = settings
    )
    model = compile_model(
        model = model,
        settings=settings,
        print_fn=printfn
    )
    
def startModelTraining(settings:ModelSettings, 
                printfn:Callable[[str],None] = print, 
                graphfn:Callable[[TypedDict],None] = print,
                figure = None):

    global model

    generators = makeGenerators(
        generatorType=GeneratorType.CSV, 
        settings=settings
    )


    model, history = train_model(
        model = model,
        train_generator=generators[0],
        valid_generator=generators[1],
        settings = settings,
        graphing_func=graphfn
    )


    model_name, graph_name = save_statistics(
        history = history,
        settings = settings
    )


    # Saves model into the folder with name specified below (adds unique id to the file name)
    save_model(
        model = model,
        model_name = model_name
    )

    #x,real = generators[1].__getitem__(0)
    #y = model(x, training=False)

    #printImagesWithBoxesModel(8,x,real,y,settings)
    figure.savefig(f"{graph_name}")

    return graph_name


    




