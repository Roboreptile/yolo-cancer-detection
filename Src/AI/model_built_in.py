from Src.settings import *

# =================
# ALEXNET MODEL
# VERSION MINI/BASIC
# ==================

def AlexNet():
    return ModelSettings(
        convolution_layers = [              
                ConvLayer(
                    filters=16,
                    size=3
                ),
                ConvLayer(
                    filters=16,
                    size=3,
                ),
                MaxPool(),
                ConvLayer(
                    filters=32,
                    size=3
                ),
                ConvLayer(
                    filters=32,
                    size=3,
                ),
                MaxPool(),
                ConvLayer(
                    filters=64,
                    size=3
                ),
                ConvLayer(
                    filters=64,
                    size=3,
                ),
                MaxPool()
            ],       

        middle_layer = MiddleLayerType.max_pool,

        dense_layers = [                    
                DenseLayer(
                    neurons = 1024,
                    drop = 0.2
                ),
                DenseLayer(
                    neurons = 1024,
                    drop = 0.2
                )
        ],

        optimizer = OptimizerType.adam,                 
        learning_rate = 0.001,              
        epochs = 20, 
        batch_size = 2048,                  
        
        model_name = 'alexy',          
        validation_split = 0.1,                       
        verbose = True,                   
        print_summary = True
    )




'''
settings = ModelSettings(
    # Defines how many convolutional layers in the network, and how many filters per layer.
    # len(convolution_layers) is how many layers are in a network.
    # After every layer, MaxPool2D is performed (exluding the last one).
    # A layer size can be a single integer, or an array of integers
    # If it is an array, there are multiple Conv2Ds before MaxPool2D
    # It is a good idea to use a numer which is a power of 2
    convolution_layers = [              
                [16,16],                
                [32,32],
                [64,64]
    ],

    # Activation to be used in the filters
    # Either 'relu', 'tanh'. 
    # Relu is much faster but the loss function might explode upwards                           
    convolution_activation = ActivationType.relu,

    # Defines filter sizes per layer.
    # Len of this array must match convolution_layers'
    # Only integers are allowed!!! + make sure it is an ODD number
    # Filter of size n is n x n matrix
    convolution_sizes = [               
                3,
                3,
                3
    ],      

    # Defines the layer that connects Conv layers with Dense layers
    # Either 'global_avg', 'maxpool','flatten'
    # global_avg is significantly faster but might lose some information 
    # maxpool is a jack of all trades
    # flatten is the best but slow as turtle
    middle_layer = MiddleLayerType.max_pool,

    # Defines how many dense layers in the network, and the neuron count.
    # len(dense_layers) is how many layers are in a network.
    # After every layer, Dropout is performed is performed (exluding the last one).
    # A layer size can be a single integer, or an array of integers
    # It is a good idea to use a numer which is a power of 2
    # Works similarly to Conv2D in that regard
    dense_layers = [                    
                1024,
                1024
    ],

    # Activation to be used in the neurons
    # Either 'relu', 'tanh'. 
    # Relu is much faster but the loss function might explode upwards            
    dense_activation = ActivationType.relu,          

    # Chance of deactivating the output of any neuron
    # It is a number between 0 and 1
    # Prevents overfitting with higher values, but harder to learn
    dropout_rate = 0.2,

    # Optimizer to be used in the network, from the following:
    # 'SGD' (momentum gradient descent),     
    # 'RMSprop' (decaying learning rate), 
    # 'adam' (both basically)
    # It is almost always a good idea to use adam
    optimizer = OptimizerType.adam,                 

    # How fast the network is learning (Speed of gradient descent)
    # Higher values make it much easier to learn at first, good for testing
    # But it is harder then to achieve decent accuracy in the long-term
    # Value between 0.0 - 0.01
    learning_rate = 0.001,              

    # How many epochs to perform
    epochs = 20, 

    # This value determines how many steps there are in a single epoch
    # Increases training time, but helps with overfitting
    # data_count/batch_size is the number of steps
    # If batch_size = 1, it is called a stochastic gradient descent
    # Which is extremely good for learning
    # But you can go asleep before a single epoch finishes
    # Also, reducing this may help memory errors, as less images are loaded into memory
    batch_size = 2048,                  
    

    # Optional, not really important
    model_name = 'alexy',               # Name of the model
    validation_split = 0.1,             # Percentage of data used for validation of learning outcomes            
    verbose = 1,                        # Either 1 (on) or 0 (off)
    print_summary = True                # Prints network summary upon creation
)
'''