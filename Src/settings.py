from typing import List
from PyQt5.QtWidgets import QLabel
from Src.const import DEBUG, DELIMETER
from enum import Enum

class ActivationType(Enum):
    relu = 1
    tanh = 2

class MiddleLayerType(Enum):
    flatten = 1
    max_pool = 2
    average_pool = 3

class OptimizerType(Enum):
    adam = 1
    SGD = 2
    RMSProp = 3
    nadam = 4

class Layer:
    def __init__(self) -> None:
        self.input_shape = []
        self.output_shape = []
        self.label:QLabel = None

class DenseLayer(Layer):
    def __init__(self, neurons = 1024, act = ActivationType.relu, drop = 0.1) -> None:
        super().__init__()
        self.neurons = neurons
        self.act = act
        self.drop = drop
        self._tn = None
        self._td = None

    
    def __str__(self):
        return f" (Neurons: {self.neurons}, Dropout: {self.drop}, Act: {self.act})"
    

class ConvLayer(Layer):
    def __init__(self, filters = 32, size = 3, stride = 1, act = ActivationType.relu, is_max = False) -> None:
        super().__init__()
        self.filters = filters
        self.shape = [size,size]
        self.stride = stride
        self.act = act
        self._tf = None
        self._ts = None
        self._tst = None
        self.is_max = is_max

    def __str__(self) -> str:
        return f"(Filters: {self.filters}, Size: {self.shape}, Stride: {self.stride}, Act: {self.act})"


class MaxPool(ConvLayer):
    def __init__(self) -> None:
        super().__init__(is_max=True)

    def __str__(self) -> str:
        return "(MaxPool)"


class ModelSettings:
    '''
    This class acts as a storage for model parameters.
    '''
    def __init__(self,
        convolution_layers = [              
            ConvLayer(
                filters=32,
                size=3
            ),
            ConvLayer(
                filters=32,
                size=3,
            ),
            MaxPool(

            ),
            ConvLayer(
                filters=32,
                size=3
            ),
            ConvLayer(
                filters=32,
                size=3,
            ),
            MaxPool(

            ),
            ConvLayer(
                filters=32,
                size=3
            ),
            ConvLayer(
                filters=32,
                size=3,
            ),
            MaxPool(

            ),
        ],                                                          
        middle_layer = MiddleLayerType.average_pool,    
        dense_layers = [                   
            DenseLayer(
                neurons = 512,
                drop = 0.1
            ),
            DenseLayer(
                neurons = 512,
                drop = 0.1
            ),
            DenseLayer(
                neurons = 512,
                drop = 0.1
            ),
        ],              
        input_shape = [224,224],
        output_shape = [10,10],                 
        optimizer = OptimizerType.adam,                              
        learning_rate = 0.001,              
        epochs = 10,                     
        batch_size = 32,                
        validation_split = 0.2,        
        model_name = 'model',
        print_summary = True,
        verbose = True
    ):
        self.input_shape:List[int] = input_shape
        self.convolution_layers:List[ConvLayer] = convolution_layers              
        self.middle_layer:MiddleLayerType = middle_layer
        self.dense_layers:List[DenseLayer] = dense_layers
        self.output_shape:List[int] = output_shape

        self.optimizer:OptimizerType = optimizer
        self.learning_rate:float = learning_rate     
        self.epochs:int = epochs  
        self.batch_size:int = batch_size           
        self.validation_split:float = validation_split
        self.model_name:str = model_name
        self.print_summary:bool = print_summary
        self.verbose:bool = verbose

        assert(len(output_shape) == 2)

        if DEBUG: 
            self.epochs = 1
            self.verbose = 1


    def __str___(self):
        return DELIMETER.join([
            str(self.input_shape),
            str(list(map(str,self.convolution_layers))),
            str(self.middle_layer),
            str(list(map(str,self.dense_layers))),
            str(self.output_shape),
            str(self.learning_rate),
            str(self.optimizer),
            str(self.epochs),
            str(self.batch_size),
            str(self.validation_split)
        ])


    @staticmethod
    def header():
        return DELIMETER.join([
            "Input Shape",
            "Conv Layers",
            "Middle Layer",
            "Dense Layers",
            "Output Shape",
            "Learning Rate",
            "Optimizer",
            "Epochs",
            "Batch Size",
            "Validation Split"
        ])